<?php

use Illuminate\Database\Seeder;

class cats_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cats')->insert([
          'name' => 'Persian',
          'description' => 'The Persian cat is a long-haired breed of cat characterized by its round face and short muzzle',
          'image' => 'persian.jpg'
        ]);
        
        DB::table('cats')->insert([
          'name' => 'British Shorthair',
          'description' => 'The British Shorthair is the pedigreed version of the traditional British domestic cat, with a distinctively chunky body, dense coat, and broad face',
          'image' => 'british.jpg'
        ]);
        
        DB::table('cats')->insert([
          'name' => 'Bengal',
          'description' => 'The Bengal cat is a domesticated cat breed created from hybrids of domestic cats, the Asian leopard cat and the Egyptian Mau, which gives them their golden shimmer',
          'image' => 'bengal.jpg'
        ]);
        
        DB::table('cats')->insert([
          'name' => 'Siamese',
          'description' => 'The Ragdoll is a cat breed with a color point coat and blue eyes. They are large and muscular semi-longhair cat with a soft and silky coat.',
          'image' => 'Siamese.jpg'
        ]);
        
        DB::table('cats')->insert([
          'name' => 'Ragdoll',
          'description' => 'The Ragdoll is a cat breed with a color point coat and blue eyes. They are large and muscular semi-longhair cat with a soft and silky coat.',
          'image' => 'Ragdoll.jpg'
        ]);
    }
}
