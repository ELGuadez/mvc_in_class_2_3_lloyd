<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public static function getAllCars()
    {
    	return self::latest()->get();
    }

    public static function getOneCar($id)
    {
    	return self::where('id', $id)->firstOrfail();
    }
}
