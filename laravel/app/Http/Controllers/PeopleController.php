<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\People;

class PeopleController extends Controller
{
    //
    public function index()
    {
        $people = People::getAllPeople();
        return view('lloyd.people', compact('people'));
    }

    public function show($id)
    {
        $people = People::getOne($id);
        return view('lloyd.peopledetails', compact('people'));
    }
}
